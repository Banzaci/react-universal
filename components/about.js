import React from 'react';

export default class AboutComponent extends React.Component {

    constructor(props){
        super(props);
        this.props = props;
        this.state = {
            num:0
        }

    }

    componentDidMount(){
        setInterval(()=>{
            this.changeNum();
        }, 1000)
    }

    onClick(e) {
        console.log('Clicked');
    }

    changeNum() {
        const num = this.state.num + 1;
        this.setState({
            num
        })
    }
  render() {
    const {num} = this.state;
    return (
        <div>
            <button onClick={ this.onClick }>Click</button>
            <p>This is the about page { num }</p>
        </div>
    );
  }
}
