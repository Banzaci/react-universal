import React from 'react';
import { Router, Route, Link } from 'react-router';

const Header = (props) => {

    return (
        <nav>
            <Link className="link" to="/">Home</Link>
            <Link className="link" to="/about">About</Link>
        </nav>
    )
}

export default Header;
