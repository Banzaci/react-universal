import React from 'react';
import Header from './header';

export default class AppComponent extends React.Component {
  render() {
    return (
      <div>
        <Header />
        { this.props.children }
      </div>
    );
  }
}
